
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mySchema = new Schema({
    username: { type: String, required: true },
    email: { type: String, required: true },
    password: { type: String, required: true },
    imageUrl: {type: String, required: false}
}) 
let users = mongoose.model("users", mySchema);
module.exports = users;
